#!/bin/bash

##############################################################
# set AOSP_OUT_ROOT_DIR and WORK_DIR to the appropriate directories.
AOSP_DIR= #~/Prog-work/AOSP/work
AOSP_OUT_ROOT_DIR= #${AOSP_DIR}/out/target/product/t-station/root
WORK_DIR= #~/Prog-work/inst-boot
##############################################################


# dir/file name in ${WORK_DIR}
RAMDISK_DIR=initrd

# dir/file name in the device.
BOOT_ROM=/dev/block/rknand_boot

# commands
SH="adb shell"
SH_B="adb shell busybox"


# check if the directories exist.
for d in AOSP_OUT_ROOT_DIR WORK_DIR
do
    if [ ! -d "${!d}" ] ; then
        echo ERROR: \"${!d}\" does not exist.
        echo ERROR: please set ${d} to the appropriate directory.
        exit 1
    fi
done



# check if some commands exist.
echo INFO: checking prerequisites.
for c in pushd popd abootimg gzip cpio find 
do
    if ! command -v $c > /dev/null ; then
        echo ERROR: $c does not exist.
        exit 1
    fi
done
#...adb version check
if adb --help |& grep -q -F '1.0.33' ; then
else
    echo "ERROR: Please use ADB version 1.0.33."
    echo "ERROR: Version 1.0.32's push command behaves differently."
    echo "ERROR: Version 1.0.31 can't connect with t-station."
    exit 1
fi
#if adb --help |& grep -F '1.0.31' > /dev/null ; then
#    echo "ERROR: adb version 1.0.31 can't connect with t-station."
#    exit 1
#fi



# check device
echo INFO: checking if the device is in recovery mode.
if ! adb devices |& grep recovery > /dev/null
then
    echo ERROR: device is not recovery mode !
    exit 1
fi





# boot.img <-- extract boot partition of t-station
echo INFO: extracting boot partition.
pushd ${WORK_DIR} > /dev/null
${SH_B} dd if=${BOOT_ROM} of=/boot.img 
adb pull /boot.img ./boot.img 
#...backup
if [ ! -f ./boot-org.img ] ; then
    cp ./boot.img ./boot-org.img
fi


# initrd-new.img <-- ramdisk of boot.img + AOSP's root
echo INFO: creating new ramdisk image.
#...initrd.img <-- ramdisk of boot.img
abootimg -x boot.img /dev/null /dev/null initrd.img /dev/null 
#...initrd-new.img <-- initrd.img + override with AOSP's root
mkdir -p ./${RAMDISK_DIR}
cd ${RAMDISK_DIR}
#gzip -dc ../initrd.img | cpio -idmv
gzip -dc ../initrd.img | cpio -idm
cp -a ${AOSP_OUT_ROOT_DIR}/* ./
find | cpio -H newc -o | gzip -1 > ../initrd-new.img
cd ..


# boot-new.img <-- boot.img + initrd-new.img
echo INFO: creating new boot image.
cp boot.img boot-new.img
abootimg -u boot-new.img -r initrd-new.img


# update t-station's boot_partition <-- boot-new.img
echo INFO: updating boot partition.
adb push ./boot-new.img /boot.img
${SH_B} dd if=/boot.img of=${BOOT_ROM} 
${SH_B} sync
popd > /dev/null


echo INFO: done.
