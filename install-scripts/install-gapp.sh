#!/bin/bash

##############################################################
# set GAPP_ZIP, WORK_DIR to the appropriate directory/file name.
GAPP_ZIP= #~/Downloads/open-source/open_gapps-arm-4.4-pico-20190430.zip
#GAPP_ZIP= #~/Downloads/open-source/open_gapps-arm-4.4-pico-20190327.zip
WORK_DIR= #~/Prog-work/gapp-test
##############################################################


#-------------------------------------------------------------
# sequence of the installation
#    on the host
#    WORK_DIR/gapps-config.txt <-- create( This is OPTIONAL, see "WORK_DIR/gapps-config.txt <-- create" )
#    WORK_DIR/[installer.sh, unzi-arm, tar-arm etc] <-- extract from GAPP_ZIP
#
#    on the target device using adb
#    /etc/fstab <-- symbolic link to fstab.rk30board
#    TARGET_WORK/ <--copy-- WORK_DIR/[gapps-config.txt, installer.sh]
#    TARGET_WORK/ <--copy-- GAPP_ZIP
#    TARGET_TMP/  <--copy-- WORK_DIR/[unzip-arm, tar-arm etc]
#
#    on the target device using adb
#    make some commands available on the target device.
#
#    on the target device
#    setup environment variables( OPENGAZIP, TMP, BINARCH, PATH, LD_LIBRARY_PATH )
#    TARGET_WORK/installer.sh <-- invoke
#
#-------------------------------------------------------------

# dir/file name in the device.
#... The install log is created in TARGET_WORK.
#... TARGET_TMP is cleared when installer.sh finished. ( so TARGET_TMP != TARGET_WORK )
TARGET_WORK=/work
TARGET_TMP=/tmp
TARGET_ZIP=gapp.zip


# commands
SH="adb shell"
SH_B="adb shell busybox"
SH_T="adb shell toolbox"


# check if the directories exist.
for d in GAPP_ZIP WORK_DIR
do
    if [ ! -e "${!d}" ] ; then
        echo ERROR: \"${!d}\" does not exist.
        echo ERROR: please set ${d} to the appropriate directory/file name.
        exit 1
    fi
done



# check if some commands exist.
echo INFO: checking prerequisites.
for c in pushd popd lzip tar
do
    if ! command -v $c > /dev/null ; then
        echo ERROR: $c does not exist.
        exit 1
    fi
done
#...adb version check
if adb --help |& grep -q -F '1.0.33' ; then
else
    echo "ERROR: Please use ADB version 1.0.33."
    echo "ERROR: Version 1.0.32's push command behaves differently."
    echo "ERROR: Version 1.0.31 can't connect with t-station."
    exit 1
fi
#if adb --help |& grep -q -F '1.0.31' ; then
#    echo "ERROR: adb version 1.0.31 can't connect with t-station."
#    exit 1
#fi


# check device
if ! adb devices | grep -q recovery
then
    echo ERROR: device is not recovery mode !
    exit 1
fi
if ! adb remount
then
    echo ERROR: remount failed !
    exit 1
fi


# WORK_DIR <-- enter
pushd ${WORK_DIR} > /dev/null


# WORK_DIR/gapps-config.txt <-- create
echo INFO: create gapps-config.txt
#...this is optional, without this the calsync app is installed.
#...To downsize install, add line 'PicoGApps' to gapps-config.txt.
#...( About gapps-config.txt, see https://github.com/opengapps/opengapps/wiki/Advanced-Features-and-Options )
cat > ./gapps-config.txt <<EOF
Include

EOF


# WORK_DIR <-- extract some(-o overwrite)
unzip -o -q ${GAPP_ZIP} -d ${WORK_DIR}



# /etc/fstab <-- symbolic link to fstab.rk30board
${SH_B} ln -s /fstab.rk30board /etc/fstab



# mkdir TARGET_WORK
echo INFO: mkdir ${TARGET_WORK} on target
if ${SH} "busybox [ -d ${TARGET_WORK} ] || echo work_dir_not_exist" | grep -q work_dir_not_exist
then
    echo INFO: ${TARGET_WORK} does not exist on target
    ${SH_B} rm    ${TARGET_WORK} > /dev/null
    ${SH_B} mkdir ${TARGET_WORK}
fi
if ${SH} "busybox [ -d ${TARGET_WORK} ] || echo work_dir_not_exist" | grep -q work_dir_not_exist
then
    echo ERROR: mkdir ${TARGET_WORK} on target failed.
    exit -1
fi



# TARGET_WORK, TARGET_TMP <-- install scripts and archive
#...TARGET_WORK <--
echo INFO: "${TARGET_WORK} <-- install scripts and archive"
for f in \
  installer.sh \
  gapps-config.txt
do
    adb push $f ${TARGET_WORK}/
done
${SH_B} chmod 755 ${TARGET_WORK}/installer.sh
adb push ${GAPP_ZIP} ${TARGET_WORK}/${TARGET_ZIP}
#...TARGET_TMP <--
echo INFO: "${TARGET_TMP} <-- executables etc"
for f in \
  app_densities.txt \
  app_sizes.txt \
  bkup_tail.sh \
  busybox-arm \
  g.prop \
  gapps-remove.txt \
  tar-arm \
  unzip-arm \
  zip-arm
do
    adb push $f ${TARGET_TMP}/
done
#...chmod
for f in \
  bkup_tail.sh \
  busybox-arm \
  tar-arm \
  unzip-arm \
  test.sh \
  zip-arm
do
    ${SH_B} chmod 755 ${TARGET_TMP}/$f
done




# make some commands available on the target device.
${SH_B} cp -a ${TARGET_TMP}/busybox-arm /sbin/
${SH} /sbin/busybox-arm --install -s /sbin




# TARGET_WORK/installer.sh <-- invoke
echo INFO: start installation
${SH_T} setenforce 0
${SH} \
"export PATH=/sbin ; \
 export TMP=/tmp ; \
 export OPENGAZIP=${TARGET_WORK}/gapp.zip ; \
 export BINARCH=arm ; \
 export OUTFD=/dev/tty ; \
 ${TARGET_WORK}/installer.sh"
echo INFO: get install log
adb pull ${TARGET_WORK}/open_gapps_debug_logs.tar.gz ./

# leave TARGET_WORK
popd > /dev/null
echo INFO: done
