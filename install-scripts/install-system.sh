#!/bin/bash

##############################################################
# set AOSP_OUT_SYSTEM_DIR to the appropriate directory.
AOSP_DIR= #~/Prog-work/aosp/work
AOSP_OUT_SYSTEM_DIR= #${AOSP_DIR}/out/target/product/t-station/system
#AOSP_OUT_SYSTEM_DIR= #${AOSP_DIR}/build_output/system
##############################################################


# dir/file name in the device.
SYSTEM_DIR=/mnt/system
DATA_DIR=/mnt/data
SYS_ROM=/dev/block/rknand_system
DATA_ROM=/dev/block/rknand_userdata


# commands
SH="adb shell"
SH_B="adb shell busybox"



# check if the directories exist.
for d in AOSP_OUT_SYSTEM_DIR
do
    if [ ! -d "${!d}" ] ; then
        echo ERROR: \"${!d}\" does not exist.
        echo ERROR: please set ${d} to the appropriate directory.
        exit 1
    fi
done



# check if some commands exist.
for c in pushd popd adb
do
    if ! command -v $c > /dev/null ; then
        echo ERROR: $c does not exist.
        exit 1
    fi
done
#...adb version check
if adb --help |& grep -q -F '1.0.33' ; then
else
    echo "ERROR: Please use ADB version 1.0.33."
    echo "ERROR: Version 1.0.32's push command behaves differently."
    echo "ERROR: Version 1.0.31 can't connect with t-station."
    exit 1
fi
#if adb --help |& grep -q -F '1.0.31' ; then
#    echo "ERROR: adb version 1.0.31 can't connect with t-station."
#    exit 1
#fi



# check device
if ! adb devices | grep -q recovery
then
    echo ERROR: device is not recovery mode !
    exit 1
fi
if ! adb remount
then
    echo ERROR: remount failed !
    exit 1
fi


# mount partition
#...system
${SH_B} mkdir -p ${SYSTEM_DIR} > /dev/null
${SH_B} mount ${SYS_ROM}  ${SYSTEM_DIR} > /dev/null
if ! ${SH_B} mount | grep -q ${SYS_ROM}
then
    echo ERROR: mount system partition failed !
    exit 1
fi
#...userdata
${SH_B} mkdir -p ${DATA_DIR} > /dev/null
${SH_B} mount ${DATA_ROM}  ${DATA_DIR} > /dev/null
if ! ${SH_B} mount | grep -q ${DATA_ROM}
then
    echo ERROR: mount userdata partition failed !
    exit 1
fi


# backup original file
if ${SH} "busybox [ -d ${SYSTEM_DIR}/org ] || echo need_backup" | grep -q need_backup
then
    echo INFO: backup original files
    ${SH_B} mkdir ${SYSTEM_DIR}/org
    for f in app bin build.prop etc fonts framework lib media preinstall priv-app tts usr vendor xbin
    do
        ${SH_B} mv ${SYSTEM_DIR}/$f ${SYSTEM_DIR}/org/
    done
else
    echo INFO: original files are already backuped.
fi



# clear directories and copy files
echo INFO: "clear(rm -r and mkdir) directories and copy files"
pushd ${AOSP_OUT_SYSTEM_DIR} > /dev/null
for d in app bin etc fonts framework lib media priv-app usr vendor xbin
do
    echo clear ${SYSTEM_DIR}/$d
    ${SH_B} rm -r  ${SYSTEM_DIR}/$d > /dev/null
    ${SH_B} mkdir  ${SYSTEM_DIR}/$d > /dev/null
    if [ -e $d ] ; then
        echo copy ${SYSTEM_DIR}/$d/
        adb push $d ${SYSTEM_DIR}/
    fi
done
for f in build.prop
do
    echo copy ${SYSTEM_DIR}/$f
    adb push $f ${SYSTEM_DIR}/
done
#...mkdir vendor/xx if it doesn't exist.
for d in vendor/media vendor/lib/mediadrm
do
    if [ ! -e $d ]
    then
        ${SH_B} mkdir -p ${SYSTEM_DIR}/$d
    fi
done
popd > /dev/null
#...use some original files(which are not built from AOSP).
for d in drm egl hw ; do ${SH_B} mkdir -p ${SYSTEM_DIR}/lib/$d ; done
for f in \
    egl/libGLES_mali.so \
    hw/audio.alsa_usb.rk30board.so \
    hw/audio.primary.rk30board.so \
    hw/audio_policy.rk30board.so \
    hw/gpu.rk30board.so \
    hw/lights.rk312x.so \
    hw/power.rk312x.so \
    modules
#...Rockchip codecs    
#   libOMX_Core.so
#   libRkOMX_Resourcemanager.so
#   libomxvpu_dec.so
#   libomxvpu_enc.so
#   librk_audio.so
#   librk_demux.so
#   librk_hevcdec.so
#   librk_on2.so
#   librk_on2.so.default
#   librk_on2.so.rmvb
#...built from AOSP source
#   libion_rk.so
#   libion.so
#   hw/audio.r_submix.default.so
#   hw/gralloc.rk30board.so
#   hw/hwcomposer.rk30board.so
#...not used in AOSP
#   librkbm.so
do
    ${SH_B} cp -a ${SYSTEM_DIR}/org/lib/$f ${SYSTEM_DIR}/lib/$f
done
for f in busybox
do
    ${SH_B} cp -a ${SYSTEM_DIR}/org/bin/$f ${SYSTEM_DIR}/bin/$f
done
#...remove hw codec if it exists
${SH_B} rm -f ${SYSTEM_DIR}/lib/libstagefrighthw.so
#...use original boot animation file.
#...this is optional( if an animation file does not exist then android logo is used.)
#${SH_B} cp -a ${SYSTEM_DIR}/org/media/bootanimation.zip ${SYSTEM_DIR}/media/


# chown
echo INFO: chown
${SH_B} chown -R 0:0    ${SYSTEM_DIR}'/app/*'
${SH_B} chown -R 0:2000 ${SYSTEM_DIR}'/bin/*'
${SH_B} chown    0:0    ${SYSTEM_DIR}'/bin/ping'
${SH_B} chown    0:3003 ${SYSTEM_DIR}'/bin/netcfg'
${SH_B} chown -R 0:0    ${SYSTEM_DIR}'/etc'
${SH_B} chown -R 1014:2000 ${SYSTEM_DIR}'/etc/dhcpcd/dhcpcd-run-hooks'

${SH_B} chown -R 0:0    ${SYSTEM_DIR}'/fonts'
${SH_B} chown -R 0:0    ${SYSTEM_DIR}'/framework'
${SH_B} chown -R 0:0    ${SYSTEM_DIR}'/lib'
${SH_B} chown -R 0:0    ${SYSTEM_DIR}'/priv-app'
${SH_B} chown -R 0:0    ${SYSTEM_DIR}'/usr'
${SH_B} chown -R 0:2000 ${SYSTEM_DIR}'/vendor'
${SH_B} chown    0:0    ${SYSTEM_DIR}'/vendor/media/*'
${SH_B} chown    0:0    ${SYSTEM_DIR}'/vendor/lib/mediadrm/*'
${SH_B} chown -R 0:2000 ${SYSTEM_DIR}'/xbin'
${SH_B} chown    0:0    ${SYSTEM_DIR}'/xbin/librank'
${SH_B} chown    0:0    ${SYSTEM_DIR}'/xbin/procmem'
${SH_B} chown    0:0    ${SYSTEM_DIR}'/xbin/procrank'
${SH_B} chown    0:0    ${SYSTEM_DIR}'/xbin/tcpdump'
${SH_B} chown    0:0    ${SYSTEM_DIR}'/xbin/su'
${SH_B} chown    0:0    ${SYSTEM_DIR}'/build.prop'


# chmod
echo INFO: chmod
${SH_B} chmod -R 755 ${SYSTEM_DIR}'/bin/*'
${SH_B} chmod -R 644 ${SYSTEM_DIR}'/app/*'
${SH_B} chmod -R 644 ${SYSTEM_DIR}'/etc/*'
${SH_B} chmod -R 755 ${SYSTEM_DIR}'/etc/dhcpcd'
${SH_B} chmod -R 555 ${SYSTEM_DIR}'/etc/ppp/*'
${SH_B} chmod -R 644 ${SYSTEM_DIR}'/framework/*'
${SH_B} chmod -R 644 ${SYSTEM_DIR}'/lib/*'
${SH_B} chmod -R 644 ${SYSTEM_DIR}'/priv-app/*'
${SH_B} chmod -R 644 ${SYSTEM_DIR}'/usr/*'
${SH_B} chmod -R 644 ${SYSTEM_DIR}'/vendor/*'
${SH_B} chmod -R 755 ${SYSTEM_DIR}'/xbin/*'
${SH_B} chmod    644 ${SYSTEM_DIR}'/build.prop'
${SH_B} chmod    o+s,g+s ${SYSTEM_DIR}'/xbin/librank'
${SH_B} chmod    o+s,g+s ${SYSTEM_DIR}'/xbin/procmem'
${SH_B} chmod    o+s,g+s ${SYSTEM_DIR}'/xbin/procrank'
${SH_B} chmod    o+s,g+s ${SYSTEM_DIR}'/xbin/tcpdump'
${SH_B} find ${SYSTEM_DIR} -type d -exec busybox chmod 755 "'{}' ';'"


# chcon
echo INFO: chcon
if ${SH_B} "[ -f /system/bin/chcon ] || echo chcon_not_exist" | grep -q chcon_not_exist
then
    ${SH_B} ln -s /system/bin/toolbox /system/bin/chcon
fi
${SH} chcon   u:object_r:system_file:s0          ${SYSTEM_DIR}'/bin/*'
${SH} chcon   u:object_r:zygote_exec:s0          ${SYSTEM_DIR}/bin/app_process
#${SH} chcon   u:object_r:clatd_exec:s0           ${SYSTEM_DIR}/bin/clatd
${SH} chcon   u:object_r:debuggerd_exec:s0       ${SYSTEM_DIR}/bin/debuggerd
${SH} chcon   u:object_r:dhcp_exec:s0            ${SYSTEM_DIR}/bin/dhcpcd
${SH} chcon   u:object_r:dnsmasq_exec:s0         ${SYSTEM_DIR}/bin/dnsmasq
${SH} chcon   u:object_r:drmserver_exec:s0       ${SYSTEM_DIR}/bin/drmserver
#${SH} chcon   u:object_r:hostapd_exec:s0         ${SYSTEM_DIR}/bin/hostapd
${SH} chcon   u:object_r:installd_exec:s0        ${SYSTEM_DIR}/bin/installd
${SH} chcon   u:object_r:keystore_exec:s0        ${SYSTEM_DIR}/bin/keystore
${SH} chcon   u:object_r:mediaserver_exec:s0     ${SYSTEM_DIR}/bin/mediaserver
${SH} chcon   u:object_r:shell_exec:s0           ${SYSTEM_DIR}/bin/mksh
${SH} chcon   u:object_r:mtp_exec:s0             ${SYSTEM_DIR}/bin/mtpd
${SH} chcon   u:object_r:netd_exec:s0            ${SYSTEM_DIR}/bin/netd
${SH} chcon   u:object_r:ping_exec:s0            ${SYSTEM_DIR}/bin/ping
${SH} chcon   u:object_r:ppp_exec:s0             ${SYSTEM_DIR}/bin/pppd
${SH} chcon   u:object_r:racoon_exec:s0          ${SYSTEM_DIR}/bin/racoon
#${SH} chcon   u:object_r:rild_exec:s0            ${SYSTEM_DIR}/bin/rild
${SH} chcon   u:object_r:runas_exec:s0           ${SYSTEM_DIR}/bin/run-as
${SH} chcon   u:object_r:sdcardd_exec:s0         ${SYSTEM_DIR}/bin/sdcard
${SH} chcon   u:object_r:servicemanager_exec:s0  ${SYSTEM_DIR}/bin/servicemanager
${SH} chcon   u:object_r:surfaceflinger_exec:s0  ${SYSTEM_DIR}/bin/surfaceflinger
${SH} chcon   u:object_r:vold_exec:s0            ${SYSTEM_DIR}/bin/vold
${SH} chcon   u:object_r:wpa_exec:s0             ${SYSTEM_DIR}/bin/wpa_supplicant
${SH} chcon   u:object_r:dhcp_system_file:s0     ${SYSTEM_DIR}/etc/dhcpcd/dhcpcd-hooks/20-dns.conf
${SH} chcon   u:object_r:dhcp_system_file:s0     ${SYSTEM_DIR}/etc/dhcpcd/dhcpcd-hooks/95-configured
${SH} chcon   u:object_r:dhcp_system_file:s0     ${SYSTEM_DIR}/etc/dhcpcd/dhcpcd-run-hooks
${SH} chcon   u:object_r:dhcp_system_file:s0     ${SYSTEM_DIR}/etc/dhcpcd/dhcpcd.conf
#${SH} chcon   u:object_r:ppp_system_file:s0      ${SYSTEM_DIR}/etc/ppp/call-pppd
#${SH} chcon   u:object_r:ppp_system_file:s0      ${SYSTEM_DIR}/etc/ppp/ip-down
#${SH} chcon   u:object_r:ppp_system_file:s0      ${SYSTEM_DIR}/etc/ppp/ip-up
${SH} chcon   u:object_r:ppp_system_file:s0      ${SYSTEM_DIR}/etc/ppp/ip-up-vpn


# format data partition if this is the first time installation.
if ${SH} "busybox [ -d ${DATA_DIR}/data/jp.ccclc.tstation.home ] || echo already_cleared" | grep -q already_cleared
then
    echo INFO: userdata partition is already cleared.
    # clear dalvik-cache
    ${SH_B} rm -f ${DATA_DIR}/data/dalvik-cache/*.dex
    ${SH_B} umount ${DATA_DIR}
else
    echo INFO: format data partition.
    ${SH_B} umount ${DATA_DIR}
    ${SH} mke2fs -T ext4 ${DATA_ROM}
fi





# sync
${SH_B} sync
${SH_B} umount ${SYSTEM_DIR}
echo INFO: done
